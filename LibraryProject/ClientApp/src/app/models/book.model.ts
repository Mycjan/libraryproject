import { Author } from "./author.model";

export class Book {
  bookId: number;
  author: Author;
  title: string;
  publicationYear: number;
  description: string;

  constructor() {}
}
