import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Book } from '../models/book.model';
import { LibraryService } from '../services/library.service';


@Component({
  selector: 'app-books-collection',
  templateUrl: './books-collection.component.html'
})
export class BooksCollectionComponent implements OnInit {
  constructor(private service: LibraryService, private toastr: ToastrService) {  }

  authorFilter: string = '';
  titleFilter: string = '';
  publicationYearFilter: string = '';

  filtredBooks: Book[] = [];

  ngOnInit(): void {
    this.service.refreshBooks().then(res => this.filterBooks());
  }

  deleteBook(id: number) {
    this.service.deleteBook(id).subscribe(res => {
      this.service.refreshBooks().then(res => this.filterBooks());
      this.toastr.success("Book successfully removed.")
    }, err => this.toastr.error(err.message));
  }

  filterBooks() {
    this.filtredBooks = this.service.books.filter(b =>
      (b.title.toLowerCase().includes(this.titleFilter.toLowerCase()))
      && (b.publicationYear.toString().toLowerCase().startsWith(this.publicationYearFilter.toLowerCase()))
      && (b.author.name.toLowerCase().startsWith(this.authorFilter.toLowerCase())
        || b.author.surname.toLowerCase().startsWith(this.authorFilter.toLowerCase())
        || (b.author.name.toLowerCase() + ' ' + b.author.surname.toLowerCase()).startsWith(this.authorFilter.toLowerCase())));
  }

}
