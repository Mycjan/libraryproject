import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Author } from '../models/author.model';
import { Book } from '../models/book.model';
import { LibraryService } from '../services/library.service';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html'
})
export class AddBookComponent implements OnInit {
  public bookForm: Book = new Book();
  public authorForm: Author = new Author();
  public selectedIdx: number = null;

  constructor(private service: LibraryService, private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.service.refreshAuthors();
  }

  addBook() {
    this.bookForm.author = this.service.authors[this.selectedIdx];
    this.service.postBook(this.bookForm).subscribe(res => {
      this.service.refreshBooks();
      this.toastr.success("Book successfully added.");
    }, err => this.toastr.error(err.message));
    this.resetBookForm();
  }

  addAuthor() {
    this.service.postAuthor(this.authorForm).subscribe(res => {
      this.service.refreshAuthors()
      this.toastr.success("Author successfully added.");
    }, err => this.toastr.error(err.message));
    this.resetAuthorForm();
  }

  resetAuthorForm() {
    this.authorForm.name = null;
    this.authorForm.surname = null;
  }

  resetBookForm() {
    this.selectedIdx = null;
    this.bookForm.author = null;
    this.bookForm.description = null;
    this.bookForm.publicationYear = null;
    this.bookForm.title = null;
  }

}
