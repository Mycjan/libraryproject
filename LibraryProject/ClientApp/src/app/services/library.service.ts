import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Author } from '../models/author.model';
import { Book } from '../models/book.model';

@Injectable({
  providedIn: 'root'
})
export class LibraryService {

  books: Book[];
  authors: Author[];

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }

  deleteBook(id: number) {
    return this.http.delete(this.baseUrl + 'api/Library/' + id);
  }

  postBook(book: Book) {
    return this.http.post(this.baseUrl + 'api/Library/PostBook', book);
  }

  postAuthor(author: Author) {
    return this.http.post(this.baseUrl + 'api/Library/PostAuthor', author);
  }

  refreshBooks() {
    return this.http.get<Book[]>(this.baseUrl + 'api/Library/GetBooks')
      .toPromise()
      .then(res => this.books = res as Book[]);
  }

  refreshAuthors() {
    return this.http.get<Author[]>(this.baseUrl + 'api/Library/GetAuthors')
      .toPromise()
      .then(res => this.authors = res as Author[]);
  }
}
